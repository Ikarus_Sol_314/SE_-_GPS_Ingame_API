sealed class GPSInterface {private static readonly String version = "0";private IMyTerminalBlock me;
public GPSInterface(IMyTerminalBlock me) {this.me=me;update();}
public Future_Boolean IsActive() {preCheck();me.CustomData+=String.Format("\r\nActive?");return new Future_Boolean(me);}
public void update() {me.CustomData = "[Gps-Scripting:IN]Version:"+version;}
private void preCheck() {if(!me.CustomData.StartsWith("[Gps-Scripting:IN]")) {update();}}
public Future_Integer AddLocalGps(String name, Vector3D position, String description = "",bool persistent = false, bool showOnHud = true) {preCheck();me.CustomData+=String.Format("\r\nAddLocal:{0}:{1}:{2}:{3}:{4}:{5}:{6}",name,description,position.X,position.Y,position.Z,persistent?"Y":"N",showOnHud?"Y":"N",-1);return new Future_Integer(me);}
public Future_Boolean RemoveLocalGps(int gpsHash) {preCheck();me.CustomData+=String.Format("\r\nRemoveLocal:{0}",gpsHash);return new Future_Boolean(me);}
public Future_Boolean Clear() {preCheck();me.CustomData+=String.Format("\r\nClear");return new Future_Boolean(me);}
public class Future {protected int line;protected IMyTerminalBlock b;protected string[] cache;
public Future(IMyTerminalBlock b) {this.line=b.CustomData.Split(new String[]{"\r\n"},StringSplitOptions.None).Length-1;this.b=b;}
public bool finished() {if (cache==null || cache.Length<=line) {cache = b.CustomData.Split(new String[]{"\r\n"},StringSplitOptions.None);}return cache[0].StartsWith("[Gps-Scripting:OUT]") && cache.Length > line;}}
public sealed class Future_Boolean:Future {public Future_Boolean(IMyTerminalBlock b):base(b) {}
public bool result() {if (!finished()) {throw new Exception("Future not finished");}return cache[line].Equals("Ok");}
}
public sealed class Future_Integer:Future {public Future_Integer(IMyTerminalBlock b):base(b) {}
public int result() {if (!finished()) {throw new Exception("Future not finished");}
try {return int.Parse(cache[line]);} catch (Exception) {return -1;}}
}
}