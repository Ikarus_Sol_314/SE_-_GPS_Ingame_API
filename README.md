# Space Engineers GPS Ingame API
###### ([The Game](http://www.spaceengineersgame.com/))

## Description at the [Steam Workshop](http://steamcommunity.com/sharedfiles/filedetails/?id=1272904212).

* [Example.cs](https://gitlab.com/Ikarus_Sol_314/SE_-_GPS_Ingame_API/blob/master/Example.cs):
Ingame script demonstrating how to use the mod / GPSInterface-class (with small documentation of the GPSInterface-class)
* [GPSInterface - Compact.cs](https://gitlab.com/Ikarus_Sol_314/SE_-_GPS_Ingame_API/blob/master/GPSInterface%20-%20Compact.cs):
A compact version of the GPSInterface-class

## GPSInterface Documentation

[GPS Ingame API - Example (Steam Workshop)](http://steamcommunity.com/sharedfiles/filedetails/?id=1272911088)